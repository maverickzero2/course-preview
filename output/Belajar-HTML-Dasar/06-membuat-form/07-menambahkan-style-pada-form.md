## Menambahkan Style pada Form

Untuk memberikan style pada form, kita dapat menggunakan CSS. Dengan CSS, form yang kita buat akan tampak lebih menarik untuk dilihat. Seperti, memberikan style pada fields input, pada button, maupun memberikan background-color, dan lain-lain. 

### Style pada Fields Input

Gunakan properti `width` untuk menentukan lebar kolom input:

```html
<style> 
input {
	width: 100%;
}
</style>

<p>Input field dengan width penuh:</p>
<form>
	<label for="fname">First Name</label>
	<input type="text" id="fname" name="fname">
</form>
```

Jika lebar input fields tidak ingin dengan width penuh, kita dapat mengurangi ukuran width-nya.

Contoh di atas berlaku untuk semua `<input>` elemen. Jika kita hanya ingin menerapkan style pada elemen `<input>` dengan tipe tertentu saja, gunakan selector seperti berikut:
- `input[type=text]` - hanya akan memilih input bertipe teks
- `input[type=password]` - hanya akan memilih input bertipe password
- `input[type=number]` - hanya akan memilih input bertipe number

### Mengatur Input Padding

Gunakan *property* `padding` untuk menambahkan ruang di dalam kolom teks. Kita juga dapat menambahkan `margin` untuk mengatur jarak antar elemen.

```html
<style> 
input[type=text] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	box-sizing: border-box;
}
</style>

<form>
	<label for="fname">First Name</label>
	<input type="text" id="fname" name="fname">
	<label for="lname">Last Name</label>
	<input type="text" id="lname" name="lname">
</form>
```

### Input Border

Gunakan *property* `border` untuk mengubah ukuran border dan warna, dan gunakan *property* `border-radius` untuk menumpulkan sudut border.

```html
<style> 
input[type=text] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	box-sizing: border-box;
	border: 2px solid pink;
	border-radius: 4px;
}
</style>

<form>
	<label for="fname">First Name</label>
	<input type="text" id="fname" name="fname">
	<label for="lname">Last Name</label>
	<input type="text" id="lname" name="lname">
</form>
```

### Field input dengan icon/gambar

Jika Kamu ingin menampilkan icon di dalam field input, gunakan *property* `background-image` dan untuk mengatur icon tersebut, gunakan *property* `background-position`.

```html
<style> 
input[type=text] {
	width: 50%;
	box-sizing: border-box;
	border: 2px solid #ccc;
	border-radius: 4px;
	margin: 8px 0;
	font-size: 16px;
	background-color: white;
	background-image: url('https://i.imgur.com/mWh41ka.png');
	background-position: 10px 8px; 
	background-repeat: no-repeat;
	padding: 12px 20px 12px 40px;
}
input[type=password] {
	width: 50%;
	box-sizing: border-box;
	border: 2px solid #ccc;
	border-radius: 4px;
	margin: 8px 0;
	font-size: 16px;
	background-color: white;
	background-image: url('https://i.imgur.com/TZkO5NZ.png');
	background-position: 10px 8px; 
	background-repeat: no-repeat;
	padding: 12px 20px 12px 40px;
}
</style>

<form>
	<input type="text" name="username" placeholder="Username">
	<input type="password" name="password" placeholder="Password">
</form>

```

Jika kita tidak menggunakan `background-repeat: no-repeat;`, maka icon yang kita gunakan akan ter-repeat di dalam field (icon akan memenuhi field input).

### Mengatur Style Select

Ubah warna latar elemen `<select>` dengan *property* `background-color` dan hilangkan dengan *property* `border="none"`.

```html
<style> 
select {
	width: 100%;
	padding: 16px 20px;
	margin: 8px 0px;
	border: none;
	border-radius: 4px;
	background-color: #ccc;
}
</style>

<form>
	<label for="jurusan">Jurusan</label>
	<select id="jurusan1" name="jurusan1">
		<option value="ti">Teknik Informatika</option>
		<option value="tm">Teknik Mesin</option>
		<option value="tp">Teknik Pendingin dan Tata Udara</option>
	</select>
</form>

```

### Mengubah Style Button

Kamu dapat mempercantik tampilan tombolmu dengan mengatur warna latar, teks, dan paddingnya. 

```html
<style> 
input[type=button], input[type=submit], input[type=reset] {
	background-color: #009b89;
	border: none;
	color: white;
	padding: 10px 20px;
	margin: 4px 2px;
}
</style>

<input type="reset" value="Reset">
<input type="submit" value="Submit">
```