## Elemen Input

Elemen yang paling penting dalam form adalah elemen `<input>`. Elemen ini memungkinkan pengunjung memasukan data melalui halaman form yang datanya kemudian akan dikirimkan ke server. 

Elemen `<input>` digunakan untuk menampilkan kolom isian dalam bentuk kotak dan sejenisnya yang dapat diisi data tertentu (seperti memasukan nama, email dan lain sebagainya). `<input>` termasuk elemen HTML yang tidak memiliki tag penutup.

Elemen `<input>` memiliki beberapa parameter yang wajib diset, diantaranya `name` untuk memberi identitas dari elemen input, `value` untuk mengeset nilai dari input, dan `type` untuk menentukan tipe input sesuai peruntukannya. Adapun tipe-tipe input yang dapat digunakan diantaranya: 

- `<input type="text" />` adalah kotak masukan biasa yang menerima input berupa teks, contohnya digunakan untuk inputan *nama*, *username*, dan inputan berupa teks pendek lainnya. 
- `<input type="password" />` dalam tampilannya sama dengan type text, namun teks yang dimasukkan akan ditampilkan dalam bentuk bintang atau bulatan hitam. 
- `<input type="checkbox" />` adalah inputan berupa *checkbox* yang dapat diceklis atau dicentang oleh pengguna. Input checkbox biasanya digunakan untuk opsi yang dapat dipilih satu atau lebih. 
- `<input type="radio" />` mirip dengan *checkbox*, namun pengguna hanya bisa memilih satu diantara pilihan group radio yang nilai atribut `name`nya sama. Contoh inputan type radio adalah jenis kelamin.
- `<input type="submit" />` akan menampilkan tombol untuk memproses form. Biasanya diletakkan pada baris terakhir dari form. Atribut `value` pada input tipe ini digunakan untuk menampilkan teks dari tombol submit.
- `<input type="reset" />` akan menampilkan tombol untuk membatalkan semua proses atau mereset/membersihkan semua input yang ada di dalam form.
- `<input type="button" />` akan menampilkan tombol untuk keperluan selain submit dan reset.

Selain itu pada HTML5, terdapat beberapa tambahan tipe input lainnya yang dapat Kamu gunakan untuk memperkaya tampilan formmu, diantaranya adalah `color`, `date`, `datetime-local`, `email`, `month`, `number`, `range`, `search`, `tel`, `time`, `url`, dan `week`.

```html runner-html
<form action="">
    Nama: <input type="text" name="nama" /><br>
	Email: <input type="email" name="email" /><br>
	Password: <input type="password" name="password" /><br>
	Kota: <input type="text" name="kota" /><br>
    Tanggal lahir: <input type="date" name="tgl" /><br>
    Gender: <input type="radio" value="l" /> Laki-laki 
    		<input type="radio" value="p" /> Perempuan <br>
    <input type="submit" />
</form>
```