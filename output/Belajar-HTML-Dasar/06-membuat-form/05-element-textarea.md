## Elemen Textarea

Elemen `<textarea>` pada dasarnya sama dengan `<input type="text">`, namun lebih besar dan dapat berisi banyak baris. Elemen ini digunakan saat *user* diharuskan memberikan rincian yang mungkin lebih panjang dari satu kalimat saja. 

```html runner-html
<label>Alamat:</label> <br>
<textarea name="alamat" rows="3" cols="50"></textarea>
```

Panjang kolom dan banyak baris untuk textarea di atur melalui atribut `cols` (untuk kolom) dan `rows` (untuk baris). Kamu juga dapat menggunakan style property `width` dan `height` untuk mengatur lebar dan tinggi elemen ini.

Berbeda dengan elemen `<input>`, elemen `<textarea>` merupakan elemen yang memiliki tag penutup. Nilai atau *value* dari elemen ini adalah teks yang disisipkan di antara tag pembuka `<textarea>` dan tag penutup `</textarea>`.

