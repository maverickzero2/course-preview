## Validasi Form

Validasi pada sebuah form memang sangat dibutuhkan, terutama validasi form ketika *user* tidak mengisi atau mengosongkan inputan. Hal ini berguna agar data yang dimasukkan ke website kita lengkap.

Untuk melakukan validasi pada inputan yang wajib diisi, kita dapat menambahkan atribut "required" ke dalam tag input kita.

```html
<input type="text" name="input" required>
```

Berikut contoh penggunaan atribut "required" pada form biodata dibawah ini:

```html runner-html
<form action="#">
    Nama: <input type="text" name="nama" required /><br>
    Email: <input type="email" name="email" required /><br>
    Password: <input type="password" name="password" required /><br>
    <input type="submit" value="Kirim" />
    <input type="reset" value="Bersihkan" />
</form>
```

Saat kita mengklik tombol "kirim" sedangkan kolom input belum diisi, akan muncul peringatan *"Please fill out this field."*.