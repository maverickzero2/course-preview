## Tombol Submit Reset

Tombol submit dalam form digunakan untuk mengirim data form ke server, sedangkan tombol reset digunakan untuk membersihkan isian yang sudah ada di dalam form. 

Untuk membuat tombol sumbit dan reset ada berbagai cara dalam HTML. Kita dapat membuat tombol tersebut dengan menggunakan tag `<input>` dengan memformat atribut type ke jenis submit ataupun reset. Seperti contoh dibawah ini:

```html runner-html
<form action="">
    Nama: <input type="text" name="nama" /><br>
    Email: <input type="email" name="email" /><br>
    Password: <input type="password" name="password" /><br>
    <input type="submit" value="Kirim" />
    <input type="reset" value="Bersihkan" />
</form>
```

Ataupun kita juga dapat menggunakan elemen khusus dengan fungsi yang sama yaitu dengan elemen `<button>`. Untuk merubah fungsi tombol tersebut menjadi tombol submit dan reset, kita hanya mengubah atribut typenya saja menjadi `<button type="submit">` atau `<button type="reset">`. Seperti contoh dibawah ini:

```html
<form action="">
    Nama: <input type="text" name="nama" /><br>
    Email: <input type="email" name="email" /><br>
    Password: <input type="password" name="password" /><br>
    <button type="submit">Kirim</button>
    <button type="reset" >Bersihkan</button>
</form>
```

Berbeda dengan tag `<input>` yang menggunakan atribut `value` untuk teks tombolnya, pada tag `<button>` memiliki tag tutup dan teks tombol disimpan di dalam elemen.



