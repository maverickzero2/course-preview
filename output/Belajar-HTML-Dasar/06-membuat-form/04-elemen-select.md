## Elemen Select

Elemen `<select>` digunakan untuk inputan yang telah tersedia nilainya dan user hanya dapat memilih dari nilai yang ada (*dropdown*). Penggunaan elemen `<select>` digunakan bersama-sama dengan elemen `<option>`, dimana elemen `<option>` digunakan untuk menampilkan daftar pilihan. Kegunaan elemen `<select>` serupa dengan kegunaan elemen `<input type="radio" />`. 

```html runner-html
<label>Golongan Darah:</label>
<select name="golongan_darah">
    <option value="A">A</option>
    <option value="B">B</option>
    <option value="AB">AB</option>
    <option value="O">O</option>
</select>
```

Elemen `<option>` memiliki atribut *value*. Nilai dari atribut inilah yang akan dikirimkan kem web server saat form disubmit, sehingga disarankan menggunakan nilai yang mencerminkan pilihan user. Nilai dari atribut `value` tidak harus sama dengan apa yang dilihat user. 

