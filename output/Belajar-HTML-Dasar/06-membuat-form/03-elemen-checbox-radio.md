## Elemen Checkbox dan Radio

*Checkbox* dan *radio* berfungsi untuk membuat opsi yang dipilih dengan cara menandainya. Checkbox digunakan bila opsi pilihan dapat dipilih lebih dari satu, sedangkan radio berfungsi untuk membuat opsi pilihan yang diisi dengan cara memilih dari salah satu tombol radio yang ada. Radio digunakan saat terdapat banyak pilihan, tetapi hanya satu pilihan yang harus dipilih.  

Dalam penggunaan checkbox dan radio di HTML, kita hanya memerlukan tag `<input>` dengan sebuah atribut `type="checkbox"` untuk membuat checkbox dan atribut `type="radio"` untuk membuat radio.

```html runner-html
<!DOCTYPE html>
<html>
<head>
  <title>Belajar HTML</title>
</head>
<body>
  <form>
    <label for="jk">Jenis Kelamin:</label><br />  
    <input type="radio" name= "pr" value="p"> Perempuan
    <input type="radio" name= "lk" value="l"> Laki-Laki
    <label for="hoby">Hobi:</label><br />
    <input type="checkbox" name="baca" value="Membaca"> Membaca
    <input type="checkbox" name="oraga" value="Olahraga"> Olahraga
    <input type="checkbox" name="mmusik" value="Musik"> Bermusik
  </form>
</body>
</html>
```

Selain atribut `type`, elemen input checkbox dan radio memiliki beberapa atribut lain, diantaranya:

* `name`, digunakan sebagai identitas dari elemen yang datanya akan dikirim ke server agar dapat dikenali.
* `value`, berisi nilai yang akan dikirim ker server dari elemen yang terpilih.
* `checked`, satu-satunya nilai dari atribut checked ini adalah checked, sehingga penulisannya menjadi: `checked="checked"`. Jika kita menggunakan atribut checked, maka pada saat halaman tampil pertama kali, kotak isian checkbox atau radio akan terisi langsung. Hal ini bisa digunakan sebagai isian default untuk form kita.

  ​



