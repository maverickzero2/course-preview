## Manipulasi Style Table 

CSS menyediakan beberapa *property* untuk memanipulasi style table menjadi lebih menarik, karena standar tampilan table pada HTML masih kurang menarik. *Property* tersebut diantaranya dapat mengatur border, warna background, margin dan padding, lebar dan tinggi kolom, dan lain-lain.

Berikut ini beberapa *property* dasar untuk mengatur table:

## border

`border` adalah *property* CSS yang digunakan untuk menetapkan border (batas/garis) sebuah tabel. 

```css
table, th, td {
  border: 1px solid red;
}
```

## border-collapse

border-collapse digunakan untuk menjadikan border antar cell dilebur menjadi single border.

```css
table {
  border-collapse: collapse;
}
```

## Width dan Height

Width dan height mendefinisikan *property* width dan height. Width digunakan untuk lebar table/kolom, sedangkan height untuk panjang table/baris. Nilai *property* untuk width dapat diisi menggunakan nilai persentase dari total lebar elemen induknya.

```css
table {
    width: 100%;
}

th {
   height: 50px;
}
```

## Table Padding

Untuk mengatur jarak antara border dan konten sel dalam table, gunakan *property* padding pada elemen `<td>` dan `<th>`.

```css
th, td {
    padding: 15px;
    text-align: left;
}
```

## Table Hover

Saat mouse kita menyoroti baris tabel `<tr>`, maka akan menampilkan efek tertentu.

```css
tr:hover {
  background-color: #f5f5f5
}
```

Warna background dari baris tabel yang disorot oleh pointer mouse akan berubah warna menjadi #f5f5f5. Perhatikan bahwa selector `:hover` juga dapat Kamu terapkan untuk elemen-elemen yang lain.

## Table Striped

Table striped atau mewarnai latar baris secara bergantian pada tabel, umumnya diterapkan untuk memudahkan melihat data pada setiap baris. Untuk tabel motif zebra, gunakan selector `tr:nth-child(even)` dan tambahkan `background-color`. Selector tersebut akan memilih elemen `<tr>` yang nomor indeksnya genap saja.

```css
tr:nth-child(even) {
	background-color: #f2f2f2
}
```

`:nth-child()` dapat diisi dengan keyword `odd` dan `even`, untuk memilih elemen yang nomor indeksnya ganjil atau genap.

## border-bottom

Gunakan property border-bottom pada elemen `<th>` dan `<td>` untuk garis yang hanya pada bagian bottom setiap baris. 

```css
th, td {
  border-bottom: 1px solid #ddd;
}
```

## Warna Tabel

Untuk merubah warna latar dan teks dari tabel gunakan *property* `background-color` dan `color` pada elemen `<table>`, `<th>` atau `<td>`.

```css
th {
   background-color: #4CAF50;
   color: white;
}
```



Kamu dapat mencoba berbagai opsi property CSS di atas pada table dan melihat efek dari setiap stylenya.

```html runner-html
<style>
table, th, td {
  border: 1px solid red;
}
</style>
<table>
	<tr>
		<th>Nama</th>
		<th>Kota</th>
		<th>Gol. Darah</th>
	</tr>
	<tr>
		<td>Toni</td>
		<td>Bandung</td>
		<td>B</td>
	</tr>
	<tr>
		<td>Kresna</td>
		<td>Jakarta</td>
		<td>AB</td>
	</tr>
	<tr>
		<td>Singgih</td>
		<td>Cianjur</td>
		<td>A</td>
	</tr>
</table>
```

