## Menggabungkan Sel ke Bawah

Kita juga dapat merentangkan sebuah sel pada tabel ke bawah, sehingga tingginya setara dengan beberapa baris, menggunakan atribut `rowspan`.

```html
<table border="1">
	<tr>
		<th>Language</th>
		<th>Framework</th>
	</tr>
	<tr>
		<td>PHP</td>
		<td>Laravel</td>
	</tr>
	<tr>
		<td></td>
		<td>CodeIgniter</td>
	</tr>
    <tr>
		<td>Ruby</td>
		<td>Ruby on Rails</td>
	</tr>
	<tr>
		<td></td>
		<td>Sinatra</td>
	</tr>
</table>

```

Kode table di atas akan ditampilkan seperti ini:

![](../images/04-04-table-rowspan.png) 

Misalkan kita ingin baris yang teksnya kosong digabungkan dengan sel di atasnya (sel berisi teks PHP dan Ruby), maka kita tinggal menggunakan atribut `rowspan` pada elemen `<td>` yang paling atas dan menghapus elemen `<td>` yang ingin digabungkan. Nilai atribut `rowspan` diisi dengan banyaknya baris yang ingin digabungkan. 

```html
<table border="1">
	<tr>
		<th>Language</th>
		<th>Framework</th>
	</tr>
	<tr>
		<td rowspan="2">PHP</td>
		<td>Laravel</td>
	</tr>
	<tr>
		<td>CodeIgniter</td>
	</tr>
    <tr>
		<td rowspan="2">Ruby</td>
		<td>Ruby on Rails</td>
	</tr>
	<tr>
		<td>Sinatra</td>
	</tr>
</table>
```

![](../images/04-04-table-rowspan-after.png) 