## Menggabungkan Sel ke Samping

Colspan adalah HTML atribut yang digunakan untuk memperlebar atau menggabungkan beberapa kolom menjadi satu, sehingga satu unit kolom ini menjadi lebih lebar. 

```html
<table border="1">
<tr>
	<th>No.</th>
	<th>Nama</th>
</tr>
<tr>
	<td>1</td>
	<td>Toni</td>
	<td>Haryanto</td>
</tr>
<tr>
	<td>2</td>
	<td>Kresna</td>
	<td>Galuh</td>
</tr>
</table>
```

Pada kode di atas jumlah kolom di tiap barisnya ada 3, kecuali baris header tabel. Hal ini akan membuat tampilannya seperti ini:

![](../images/04-03-table-colspan.png) 

Kolom ketiga tidak memiliki header, padahal kita ingin header **Nama** digunakan sebagai header untuk kolom kedua dan ketiga sekaligus. Untuk itu kita dapat menambahkan atribut `colspan` pada elemen `<th>` untuk header **Nama**.

```html
<table border="1">
<tr>
	<th>No.</th>
	<th colspan="2">Nama</th>
</tr>
<tr>
	<td>1</td>
	<td>Toni</td>
	<td>Haryanto</td>
</tr>
<tr>
	<td>2</td>
	<td>Kresna</td>
	<td>Galuh</td>
</tr>
</table>
```

Pada contoh di atas, atribut colspan bernilai 2, yang artinya kita merentangkan/menggabungkan 2 sel kolom ke samping menjadi satu sel.

![](../images/04-03-table-cospan-after.png) 