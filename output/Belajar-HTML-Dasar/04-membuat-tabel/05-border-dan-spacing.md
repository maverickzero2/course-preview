## Border dan Spacing

Atribut `border`, `cellspacing` dan `cellpadding` digunakan untuk mengubah tampilan tabel, terutama berkaitan dengan border dan spasi diantara sel tabel.

Atribut `border` digunakan untuk mengatur ketebalan dari garis tepi (border) dari tabel. Jika tidak diberikan atribut ini, maka web browser akan menampilkan tabel tanpa garis tepi. 

Nilai dari atribut ini berupa angka yang diukur dalam satuan pixel. Jika kita memberikan nilai `border="1"`, maka web browser akan menampilkan garis tepi sebesar 1 pixel pada semua sisi dari tabel dan juga untuk setiap sel tabel.

Atribut `cellspacing` digunakan untuk mengatur jarak antara garis tepi luar table (border) dengan garis sel. Nilai dari atribut ini berupa angka yang diukur dalam satuan pixel.

Attribut `cellpadding` digunakan untuk mengatur jarak antara konten di dalam sel dengan garis sel.

Kamu dapat melihat hasil dari tampilan table di bawah ini dan mencoba mengganti nilai dari setiap atributnya untuk melihat perbedaannya.

```html runner-html
<table border=1 cellspacing=10 cellpadding=10>
	<tr>
		<th>Nama</th>
		<th>Kota</th>
		<th>Gol. Darah</th>
	</tr>
	<tr>
		<td>Toni</td>
		<td>Bandung</td>
		<td>B</td>
	</tr>
	<tr>
		<td>Kresna</td>
		<td>Jakarta</td>
		<td>AB</td>
	</tr>
	<tr>
		<td>Singgih</td>
		<td>Cianjur</td>
		<td>A</td>
	</tr>
</table>
```

