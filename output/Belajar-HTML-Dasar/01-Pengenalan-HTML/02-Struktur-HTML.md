## Struktur HTML

Teks HTML ditulis di dalam sebuah dokumen HTML. Dokumen HTML memiliki struktur minimal sebagai berikut:

```html
<!DOCTYPE html>
<html>
    <head>
	    <title>Belajar HTML</title>
    </head>
    <body>
	    <h1>Ini Judul Konten</h1>
	    <p>Ini paragraf konten.</p>
    </body>
</html>
```

* Deklarasi `<!DOCTYPE> ` adalah tag yang berfungsi untuk menginformasikan pada browser tentang versi HTML yang sedang digunakan. Versi HTML saat ini adalah HTML5 dan menggunakan deklarasi tag `<!DOCTYPE html>`.

* Tag `<html>` ini membungkus dokumen HTML yang terdiri dari header dokumen yang diwakili oleh `<head></head>` dan badan dokumen yang diwakili oleh tag `<body></body>`.
* Tag `<head>` berisi berbagai informasi dari dokumen HTML.
* Tag `<title>` adalah salah satu contoh informasi yang terdapat di dalam tag `<head>`. `<title>` akan menampilkan teks pada judul browser atau pada tab browser.
* Tag `<body>` adalah induk dari konten halaman yang akan ditampilkan. 
* Tag `<h1>` dan `<p>` adalah contoh tag untuk menampilkan konten halaman. `<h1>` untuk menampilkan judul, dan tag `<p>` untuk menampilkan paragraf.
