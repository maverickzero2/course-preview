## Elemen HTML

**Elemen HTML** tersusun dari tag pembuka, tag penutup, dan konten yang disimpan diantara keduanya.

**Tag HTML** adalah batas dari sebuah elemen HTML. Tag ditulis di antara tanda kurung sudut `<` dan `>`, misalnya `<h1>`. Tag penutup ditulis seperti tag pembuka, dengan diawali karakter garis miring `/` sebelum nama tagnya, misalnya `</h1>`. Diantara tag pembuka dan tag penutup dapat kita isi dengan konten berupa teks atau elemen HTML lain.

Berikut ini adalah format penulisan sebuah elemen HTML:

```
<namatag> Konten elemen </namatag>
```

Tag mendefinisikan maksud konten yang ada di dalamnya. Ketika kita menuliskan sebuah konten menggunakan tag `<h1>`, misalnya `<h1>Halo Coders!</h1>` berarti kita mendefinisikan konten "Halo Coders!" sebagai sebuah *heading* atau judul. Ketika kita menulis konten di dalam tag `<p>` maka berarti kita mendefinisikan konten tersebut sebagai sebuah paragraf.

Tag sendiri tidak akan ditampilkan di browser. Tag hanya menjadi pengenal untuk browser dalam menentukan seperti apa sebuah konten ditampilkan. Misalnya, ketika kita menulis `<strong>tulisan dengan penekanan</strong>`, maka konten di dalam tag tersebut akan ditampilkan sebagai tulisan yang dicetak tebal.

### Urutan Menutup Tag

Setiap tag HTML ditutup berdasarkan urutan terakhir ia dibuka. Perhatikan contoh berikut:

```html
<h1>Membangun <strong>cara berfikir <em>struktural</strong></em> dengan pemrograman</h1>
```

Pada kode di atas, tag tutup `</strong>` berada di dalam elemen `<em></em>`, padahal tag buka `<strong>` disimpan di luar elemen `<em></em>`. Meskipun kode di atas tetap menghasilkan tampilan yang sesuai dengan yang kita inginkan, tetapi sebaiknya tag pembuka dan penutup diposisikan sesuai dengan urutan turunannya. Kode di atas sebaiknya ditulis seperti ini:

```html
<h1>Membangun <strong>cara berfikir <em>struktural</em></strong> dengan pemrograman</h1>
```


### Elemen Tanpa Tag Tutup

Meski demikian tidak semua elemen HTML memerlukan konten. Ada juga elemen yang tidak memerlukan konten, diantaranya elemen `<br />`, `<hr />`, dan `<img />`. Elemen `<br />` adalah elemen HTML untuk menuju baris baru (*line break*). Elemen `<hr />` adalah elemen HTML untuk membuat garis horizontal (*horizontal rule*). Elemen `<img />` adalah elemen HTML untuk menampilkan gambar pada halaman. 

Karena tidak memiliki konten, maka ia tidak memerlukan tag penutup, sehingga penulisannya adalah dengan menutup dirinya sendiri dengan diakhiri karakter ` />`.

```html
<br />
<hr />
<img src="image.jpg" />
```

Tag tanpa tutup juga dapat ditulis tanpa garis miring di akhirnya:

```html
<br>
<hr>
<img src="image.jpg">
```