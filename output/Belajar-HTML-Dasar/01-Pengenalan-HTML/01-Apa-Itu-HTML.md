# Apa Itu HTML?

HTML adalah bahasa yang digunakan untuk membuat website. Setiap website yang ada di dunia ini dibangun menggunakan bahasa yang dinamakan HTML. HTML menjadi sebuah kerangka membentuk struktur sebuah halaman website.

HTML adalah singkatan dari **HyperText Markup Language**.

- **HyperText** adalah teks yang berhubungan dengan teks lain, atau teks informasi yang berisi referensi ke informasi atau dokumen lain melalui tautan atau link. Link yang tersedia pada halaman tersebut disebut dengan *hyperlink*.
- Disebut **Markup Language** karena HTML menggunakan tanda (*mark*), untuk menandai bagian-bagian dari teks. Misalnya, teks yang berada di antara tanda tertentu akan menjadi tebal, cetak miring, dan lain sebagainya. Tanda ini akan kita kenal di HTML dengan istilah tag.

Sejak pertama kali web ditemukan, sudah muncul dan berkembang versi dari HTML, yakni bermula dari versi 1 di tahun 1991. Kemudian HTML 2.0 lahir pada tahun 1995 dan HTML 3.2 pada tahun 1997. HTML 4.01 muncul pada tahun 1999 dan disempurnakan dengan XHTML pada tahun 2000. Versi terakhir HTML adalah HTML5 yang diperkenalkan pada tahun 2014 dan digunakan hingga saat ini.

HTML membuat sebuah teks disajikan dengan lebih kaya, baik dari sisi tampilan, maupun informasi.

Contoh sederhana HTML:
```html
<!DOCTYPE html>
<html>
    <body>
        <h1>Dokumen HTML</h1>
        <p>
            Ini adalah <em>Contoh Teks</em> di <strong>HTML</strong>
        </p>
    </body>
</html>
```

akan ditampilkan seperti ini:

![](../images/01-01-first-html.png) 

Semua website yang Kamu dapati di internet dibangun menggunakan HTML, termasuk website-website populer seperti Facebook.com, Twitter.com, Youtube.com, Google.com dan Codepolitan.com :)