## Layout Tanpa Table

Tableless atau tanpa tabel adalah format layout yang saat ini banyak digunakan. Untuk membuat layout halaman tanpa tabel yaitu menggunakan CSS dan elemen `<div>` sebagai pengganti fungsi elemen `<table>`.

Elemen `<div>` atau division (divisi/bagian) digunakan untuk menampung beberapa elemen menjadi satu bagian. Tujuannya agar mempermudah kita memberi style pada setiap bagiannya, yang kemudian diberi atribut `id`, `class`,  dan lain sebagainya. Misalnya, sebuah halaman website terdiri dari 4 bagian: header, sidebar, content dan footer. Untuk mengelompokannya kita dapat menggunakan elemen div yang diberi atribut `id` . 

```html
<div id="header">HEADER</div>
<div id="sidebar">SIDEBAR</div>
<div id="content">CONTENT</div>
<div id="footer">FOOTER</div>
```

Dari elemen-elemen `<div>` tersebut, kita dapat memberi style untuk setiap bagian dari elemen-elemen tersebut. Seperti contoh di bawah ini:

```html runner-html
<style>
#header {
	background: #00CCFF; 
	height: 10%; 
	font-size: 1.5em; 
	text-align: center; 
}
#sidebar { 
	background: #99CCFF;
	float: left; 
	height: 100px; 
	width: 30%; 
	font-size: 1.5em; 
	text-align: center;
}
#content { 
	background: #9999FF; 
	float: right; 
	width: 70%; 
	height: 100px; 
	font-size: 1.5em; 
	text-align: center;
}
#footer { 
	background: #3366FF;
	clear: both; 
	font-size: 1.5em; 
	text-align: center;
}
</style>

<div id="header">HEADER</div>
<div id="sidebar">SIDEBAR</div>
<div id="content">CONTENT</div>
<div id="footer">FOOTER</div>
```

Dari style di atas, elemen `<div>` memiliki style masing-masing berdasarkan nama id. Bagian utama yang membuat elemen `<div>` ini berlaku sebagai layout adalah properti `float` yang diisi nilai `left` bila akan ditarik ke sebelah kiri dan `right` bila hendak ditarik ke sebelah kanan. Properti `float` diterapkan pada bagian sidebar dan content, karena kedua bagian ini dipasang berdampingan. 

Terakhir, bagian `footer` mesti dikenakan style clear: both agar tidak dipengaruhi oleh elemen yang bersifat float di atasnya.