## Responsive Dasar

Desain responsive merupakan sebuah teknik desain yang membuat sebuah halaman website dapat tampil dengan baik jika dibuka di berbagai browser dengan ukuran layar yang berbeda (tanpa scroll ke samping) seperti desktop, smartphone maupun tab.

Untuk membuat website responsive, kita tambahkan elemen *media query* pada `<style>`. *Media query* digunakan untuk mendefinisikan CSS untuk ukuran layar tertentu. Artinya CSS yang ditulis di dalam blok `@media` hanya akan digunakan bila ukuran layar yang digunakan sesuai dengan yang tercantum.

```css
@media screen and (max-width: 420px) {
 
    body { background-color: lightblue;}

}
```

Kode di atas adalah contoh potongan kode dari media query. Jika ukuran lebar layar kurang dari 420px, maka warna elemen `<body>` akan berubah menjadi lightblue. Lebar layar di atas 420px tidak akan dikenakan style tersebut.

Selain `max-width`, ada juga `min-width` yang berfungsi untuk membatasi penggunaan CSS hanya untuk ukuran layar minimum tertentu.

```css
@media screen and (min-width: 160px) and (max-width: 320px) {
   
   body { background-color: #fcf; }

}
```

Perhatikan bahwa bia Kamu hendak menerapkan media query, Kamu harus menambahkan tag `<meta>` seperti pada kode di bawah ini pada bagian elemen HTML `<head></head>` agar desain responsive dapat tampil dengan sempurna pada layar berukuran kecil.

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```
