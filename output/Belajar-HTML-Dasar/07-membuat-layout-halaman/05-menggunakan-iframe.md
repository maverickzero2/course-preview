## Menggunakan iframe

HTML elemen `<iframe>`  digunakan untuk menampilkan halaman website lain pada dokumen HTML.

```html
<iframe src="http://domain.com" height="500px" width="100%"></iframe>
```

Tag `<iframe>` sering digunakan untuk menyisipkan konten dari website lain, contohnya menyisipkan script iklan dari website pihak ketiga, memasukkan widget atau aplikasi tertentu, dan memasukkan video dari sumber lain (seperti Youtube, Vimeo dan sejenisnya).

```html runner-html
<!DOCTYPE html>
<html>
  <head>
    <title>Belajar HTML</title>
  </head>
  <body>
    <h1>Belajar HTML Elemen Iframe</h1>	
    <p>Menyisipkan Peta Lokasi Kantor CodePolitan</p>
	<iframe width="560" height="315" 
     src="https://www.youtube.com/embed/sd0npbBHYAs?rel=0" 
     frameborder="0" allow="autoplay; encrypted-media" 
     allowfullscreen></iframe>
  </body>
</html>
```

Atribut `src` digunakan untuk meletakkan url halaman website yang ingin kita tampilkan. Sedangkan atribut `height` dan `width` digunakan untuk mengatur lebar dan tinggi dari elemen `<iframe>`.