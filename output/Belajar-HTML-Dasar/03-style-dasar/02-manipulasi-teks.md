## Manipulasi Teks

Dalam pembuatan website kita pasti akan mengatur ukuran font atau teks. Untuk mengatur ukuran font atau teks kita dapat menggunakan property `font-size`.

```html
<p style="font-size:20px;">Selamat belajar HTML</p>
```

Pada contoh kode di atas, kita mengeset ukuran font sebesar `20px`. `px` adalah singkatan dari pixel, yakni salahsatu satuan ukuran pada layar monitor.

Kita juga dapat mengatur perataan teks seperti rata kiri, rata kanan, rata tengah atau rata kiri-kanan dengan menggunakan property `text-align`. Property `text-align` memiliki 4 nilai yang bisa dipilih, yakni: `left`, `right`, `center`, atau `justify`. Sesuai dengan namanya, kita menggunakan `text-align:left` untuk membuat rata teks kiri dan `text-align:right` untuk rata teks kanan. Untuk membuat teks rata tengah kita mengunakan value `center` dan untuk rata kiri-kanan kita gunakan value `justify`.

```html
<p style="text-align:center;">Selamat belajar HTML</p>
```

Berikut adalah contoh langsung penggunaan property `font-size` maupun property `text-align` .

```html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
</head>
<body>
	<h1 style="font-size:24px; text-align:center;" >Belajar HTML Dasar</h1>
  	<p style="font-size:18px;">Selamat belajar style dasar pada HTML</p>
</body>
</html>
```

