## Border Elemen

Membuat garis tepi (border) merupakan salah satu efek tampilan yang paling sering ditambahkan ketika mendasain web. Kita dapat menampilkan border dari sebuah elemen dengan menggunakan property `border`. Border adalah garis batas luar dari sebuah elemen HTML.

Format penulisan property `border` adalah sebagai berikut:

``` 
border: width style color;
```

Property `border` dapat menerima 3 buah nilai secara berurutan dari *width*, *style* dan *color*. *width* adalah ukuran lebar atau ketebalan border, *style* adalah jenis tampilan border dan *color* adalah warna border.

```html
<p style="border:1px solid black;">Belajar HTML Dasar</p>
```

### Jenis Tampilan Border

`solid` adalah salahsatu jenis tampilan border, yakni garis lurus biasa. Terdapat beberapa opsi jenis tampilan border lainnya, diantaranya:

| Nilai       | Deskripsi                        |
| ----------- | -------------------------------- |
| none/hidden | tanpa border                     |
| solid       | border dengan garis tegas        |
| dotted      | border dengan titik              |
| dashed      | border dengan strip              |
| double      | border dengan garis ganda        |
| groove      | border 3D seperti lembah/parit   |
| ridge       | border 3D seperti punggung bukit |
| inset       | border 3D seperti cekungan       |
| outset      | border 3D seperti tonjolan       |

Kamu dapat mencobakan setiap style pada kolom editor di bawah ini untuk melihat hasil tampilannya.

### Sisi Border

Umumnya setiap elemen HTML memiliki 4 buah sisi, yakni sisi atas, bawah, kiri dan kanan. Ketika kita membuat style border pada elemen, akan muncul garis border di keempat sisi elemen membentuk kotak.

Kita dapat menampilkan hanya sisi tertentu saja dari border, dengan menggunakan property `border-top` untuk border atas, `border-right` untuk border kanan, `border-bottom` untuk border bawah dan `border-left`untuk border kiri.

```html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
</head>
<body>
  	<p style="border:5px solid orange;">Contoh paragraf dengan border</p>
    <p style="border-top: 1px solid red; 
    	border-bottom: 1px solid red;">Contoh paragraf dengan border 
    	atas bawah saja</p>
</body>
</html>	
```