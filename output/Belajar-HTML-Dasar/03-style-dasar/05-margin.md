## Spasi Luar Elemen (Margin)

Margin adalah jarak atau spasi luar sebuah elemen yang memisahkannya dengan elemen yang lain.

```html
<div style="margin:10px;">Contoh teks</div>
```

Contoh kode di atas akan menampilkan elemen `<div>` yang mengambil jarak sebesar 10px di atas, kanan, bawah dan kirinya.

Sama seperti property `padding`, property `margin` juga dapat diisi dengan satu hingga 4 nilai untuk mengatur ukuran margin tiap sisi-sisinya. `margin-top` untuk mengatur margin bagian atas, `margin-bottom` untuk mengatur margin bagian bawah, `margin-left` untuk mengatur margin bagian kiri dan `margin-right` untuk mengatur margin bagian kanan. 

Di bawah ini adalah contoh penggunaan property `margin` dengan mengatur salah satu sisi:

```html runner-html
<!DOCTYPE html>
<html>
<head>
    <title>Belajar HTML</title>
</head>
<body>
    <h1>Belajar HTML Dasar</h1>
    
    <div style="border:1px solid blue; margin-bottom:10px;">
        Bagian ini mengambil jarak 10px di bagian bawah.
        
        <div style="border:1px solid red; margin:10px;">
            Sub bagian ini mengambil jarak 10px dari elemen induk
        </div>
    </div>
    
    <div style="border:1px solid blue;">
        Selamat belajar HTML
    </div>
</body>
</html>
```



Seperti halnya `padding`, kita juga dapat menggunakan penulisan singkat untuk nilai properti `margin` yang dapat mengatur seluruh sudut elemen dalam satu deklarasi.

```css
margin: 10px 9px 8px 7px;
```

Sintaks margin di atas digunakan untuk mengatur seluruh jarak tepi elemen. **10px** untuk jarak margin atas, **9px** untuk jarak margin kiri, **8px** untuk jarak margin bawah, dan **7px** untuk jarak margin kanan.

