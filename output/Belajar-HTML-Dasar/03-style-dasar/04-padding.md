## Spasi Dalam Elemen (Padding)



Padding adalah property style yang dapat kita gunakan untuk memberi spasi antara konten di dalam elemen dengan border. Padding sangat bermanfaat untuk membuat tampilan menjadi lebih nyaman dilihat.

Coba bandingkan teks ini:

![](https://static.cdn-cdpl.com/source/634a156d2b7858558437c2588d26eb2f/Capture1.png) 

dengan ini:

![](https://static.cdn-cdpl.com/source/634a156d2b7858558437c2588d26eb2f/Capture2.png) 

Yang manakah yang lebih enak dilihat? Tentu yang kedua, karena kotak kedua memiliki jarak antara batas kotak dengan konten teksnya. Disitulah peran `padding`, yakni spasi di antara border dan konten elemen. Property `padding` diisi dengan nilai ukuran jarak spasi yang diinginkan. 

```html
<p style="padding:10px">
    Contoh paragraf dengan padding 
</p>
```



### Padding di Sisi Tertentu

Ketika kita menulis `padding: 15px;` maka spasi akan diterapkan di semua sisi elemen. Bila kita ingin mengatur padding hanya pada sisi tertentu saja, kita dapat menggunakan property `padding-left` untuk padding kiri, `padding-top` untuk padding atas, `padding-right` untuk padding kanan dan `padding-bottom` untuk padding bawah. 

Contoh penggunaan `padding-left` dan `padding-right` seperti berikut:

```html
<p style="padding-left:10px; padding-right:10px;">
    Contoh paragraf dengan padding 
</p>
```




Ada cara lain yang lebih ringkas untuk menentukan ukuran padding dari masing-masing sisi, yakni dengan menggunakan property `padding` dan mengisinya dengan beberapa nilai dengan urutan atas-kanan-bawah-kiri. Perhatikan contohnya pada tabel berikut:

| Nilai | Hasil |
|-------|-------|
| `padding: 15px;`    | padding semua sisi 15px    |
| `padding: 15px 8px;`    | padding atas dan bawah 15px, kanan dan kiri 8px    |
| `padding: 15px 8px 4px;`    | padding atas 15px, kanan dan kiri 8px, bawah 4px    |
| `padding: 15px 8px 4px 2px;` | padding atas 15px, kanan 8px, bawah 4px, kiri 2px    |

````html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
</head>
<body>
  	<p style="text-align:justify; 
              border:1px solid blue; 
              padding: 30px 10px;">
    	Selamat belajar style dasar pada HTML di website CodePolitan. Ikuti juga tutorial lainnya.
    </p>
</body>
</html>
````