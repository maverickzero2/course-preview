## Internal dan Embedded Style

Apa yang terjadi bila kita harus memberikan atribut style untuk setiap elemen HTML yang ada di dokumen? Tentu dokumen HTML kita akan nampak berantakan. Untuk itu kita dianjurkan untuk memisahkan style di tempatnya tersendiri, yakni di dalam tag `<style>`. Hal ini dikenal dengan istilah internal style atau internal CSS (Cascading Style Sheet).

Metode Internal Style Sheets atau disebut juga Embedded Style Sheets digunakan untuk memisahkan kode CSS dari tag HTML namun tetap dalam satu halaman HTML. Atribut `style` yang sebelumnya berada di dalam tag, dikumpulkan pada konten elemen `<style>`. Tag `style` ini harus berada pada bagian `<head>` dari halaman HTML.

```html
<style>
p { background:black; color:white; }
</style>
```

Pada contoh di atas, kita mendefinisikan *style* untuk elemen paragraf. `p` pada kode di atas disebut dengan ***selector***, yakni bagian dari CSS untuk menentukan elemen mana yang akan dikenai style. Style untuk elemen ditulis di dalam kurung kurawal setelah *selector*.

Karena elemen yang ditarget adalah `p`, maka semua elemen paragraf di dalam dokumen HTML akan dikenai style ini. Dengan demikian kita tidak perlu lagi menulis atribut style yang sama berulang untuk setiap elemen paragraf.

Kita juga dapat menuliskan style untuk banyak elemen.

```html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
    <style>
    h1 {
  		text-align:center; 
  		color:blue;
		}
	p {
 	   border: 1px solid orange; 
       padding:10px; 
       margin-bottom:10px;
	   }
    </style>
</head>
<body>
	<h1>Belajar HTML</h1>
  	<p>Selamat belajar style dasar pada HTML.</p>
    <p>Tinggal sedikit lagi untuk menguasai HTML dasar.</p>
</body>
</html>
```



Pada contoh kode di atas, kita mendefinisikan style untuk elemen paragraf dan juga elemen heading 1.