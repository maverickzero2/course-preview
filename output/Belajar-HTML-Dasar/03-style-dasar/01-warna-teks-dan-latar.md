## Mengubah Warna Elemen

Kita dapat memberikan gaya atau biasa disebut *style* pada sebuah elemen dengan menggunakan atribut `style`. Teknik ini dikenal dengan istilah **inline style**. Penulisan nilai dari artibut style adalah sebagai berikut:

```
<namatag style="property:value;">
```

Style ditulis dengan format `property:value` dimana ***property*** adalah nama jenis *style* yang akan diterapkan pada elemen dan ***value*** adalah nilai dari *property* tersebut. Misalnya kita akan mengubah warna teks menjadi oranye, maka kita tuliskan *style* seperti ini:

```html
<h1 style="color:orange">Judul Utama</h1>
```

Kita dapat menerapkan lebih dari satu style dengan memisahkan antar style dengan tanda titik koma `;`.

```html
<h1 style="color:orange; background-color:yellow">Judul Utama</h1>
```

Kode di atas akan membuat elemen heading 1 berwarna teks oranye dan berwarna latar kuning.

*Property* `color` digunakan untuk mengatur warna teks. `orange` adalah nilai style untuk warna oranye. Nilai bawaan dari *property* `color` adalah hitam. Kamu dapat menggunakan nilai lain untuk warna seperti *red*, *black*, *blue*, atau menggunakan kode HEX untuk warna seperti `#FF0000` untuk warna merah dan `#800080` untuk warna ungu. Lebih lengkap nama-nama warna yang dikenali HTML dapat dirujuk pada halaman [HTML Color Names](https://www.w3schools.com/colors/colors_names.asp)

Selain itu, kita juga dapat mengganti warna latar dari sebuah elemen dengan menggunakan *property* `background` atau `background-color`. Warna bawaan dari latar elemen adalah `transparent` atau transparan. Kita dapat mengisi nilai *property* `background` dengan nilai warna yang sama seperti saat kita mengisi nilai *property* `color`. 

```html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
</head>
<body>
    <h1 style="background:orange">Styling di HTML</h1>
  	<p style="color:red;">Selamat belajar style dasar pada HTML</p>
</body>
</html>

```

