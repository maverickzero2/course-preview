## Mengubah Nilai Style

Untuk merubah style dari elemen HTML, gunakan sintaks di bawah ini:

```js
document.getElementById(id).style.properti style="style baru"
```

Contoh berikut mengubah style elemen `<h1>`:

```html runner-html
<!DOCTYPE html>
<html>
<body>
  <h1 id="judul">Halo Coder!</h1>
  <button onclick="gantiWarna()">Ubah Warna</button>

  <script>
    function gantiWarna(){
      document.getElementById("judul").style.color ="red";
    }
  </script>
</body>
</html>
```

- Dokumen HTML di atas berisi elemen `<h1>` dengan id = "judul"
- Kita menggunakan DOM HTML untuk mendapatkan elemen dengan id = "judul"
- JavaScript mengubah style warna teks dari elemen `<h1>` dari warna default menjadi warna merah.

