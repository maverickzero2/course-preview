## Membuat Fungsi

Pada bagian sebelumnya kita sudah menggunakan fungsi. Fungsi adalah sekumpulan blok kode yang dapat dijalankan ketika dipanggil. Cara mendefinisikan fungsi di JavaScript adalah dengan menggunakan kata kunci `function` diikuti oleh nama fungsinya.

```javascript
function namafungsi(parameter) {
   // kode
}
```

Sebuah fungsi membungkus satu atau lebih baris kode. Setiap kali kita memanggil fungsi, maka kode yang ada di dalam fungsi tersebut akan dijalankan. Cara memanggil fungsi adalah dengan menuliskan nama fungsinya diikuti parameternya, contohnya `namafungsi();`. Terdapat empat komponen yang membangun fungsi, yaitu:

1. Kata kunci `function`, yang memberitahu Javascript bahwa kita akan membuat fungsi.
2. Nama fungsi, yang nanti akan kita gunakan untuk memanggil fungsi tersebut.
3. Deklarasi nama fungsi selalu diikuti tanda kurung `()`. Tanda kurung ini dapat digunakan untuk mendefinisikan parameter atau variabel yang dibutuhkan oleh kode di dalam fungsi. Tanda kurung tetap dituliskan meskipun fungsi tidak memerlukan parameter.
4. Blok kode ditulis di dalam kurung kurawal`{ }`.

Contoh deklarasi fungsi dapat dilihat pada kode berikut: 

```javascript
function katakanHalo(nama) {
   document.write("Halo " + nama);
}
```
Untuk menjalankan kode yang ada di dalam fungsi di atas, kita tinggal panggil nama fungsinya:

```javascript
katakanHalo();
```

Nama fungsi bersifat *case sensitive*, artinya huruf kapital dan huruf kecil dianggap dua nama yang berbeda. Bila Kamu memanggil fungsi di atas dengan menuliskan `katakanhalo()` (dengan huruf h kecil) maka fungsi tersebut tidak akan dijalankan.