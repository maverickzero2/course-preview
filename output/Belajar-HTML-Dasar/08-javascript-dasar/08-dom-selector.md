## DOM Selector

DOM selector digunakan untuk mengambil elemen DOM. Untuk mengambil elemen tersebut, kita gunakan `querySelector` . `querySelector` berfungsi untuk mengambil element pertama yang sesuai dengan selector CSS yang ditentukan (seperti: class, id, nama tag).

## Syntax

```
document.querySelector(CSS selectors)
```

Contoh penggunaannya seperti berikut:

Mendapat elemen `<p>` pertama dalam dokumen dengan class = "contoh":

```html runner-html
<!DOCTYPE html>
<html>
<body>

  <h2 class="contoh">Heading dengan class="contoh"</h2>
  <p class="contoh">Paragraf dengan class="contoh".</p> 


  <button onclick="myFunction()">Klik disini</button>

  <script>
    function myFunction() {
      document.querySelector("p.contoh").style.backgroundColor = "red";
    }
  </script>

</body>
</html>

```



`document.querySelector` berfungsi untuk mencari elemen DOM pertama yang sesuai dengan aturan *selector* CSS yang diberikan ke fungsi, sedangkan `p.contoh` adalah sebuah selector CSS. Dimana `p` sebagai elemen dan `contoh` sebagai class.

Tetapi, jika kita ingin mengambil semua elemen yang memenuhi syarat selector CSS, gunakan `querySelectorAll`.