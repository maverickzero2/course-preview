## Menampilkan Popup Konfirmasi

Popup konfirmasi digunakan untuk mengambil persetujuan pengguna atas suatu pilihan akasi. Contohnya, ketika pengguna mengedit data pada sebuah form kemudian pengguna hendak menutup browser tanpa mengirim data pada form, maka umumnya website akan menampilkan popup konfirmasi untuk mengingatkan bahwa ada data yang belum dikirim atau disimpan. 

Pop up konfirmasi muncul dengan menampilkan dua tombol: *OK* dan *Cancel*. Untuk lebih detail, berikut adalah contoh penggunaan popup konfirmasi:

```html runner-html
<html>
<body>
  <p>Klik tombol untuk mengkonfirmasi: </p>  
  <button onclick="konfirmasi();">Konfirmasi</button>
  
  <script type="text/javascript">
    function konfirmasi(){
      var konf = confirm("Apakah anda ingin menyimpan password untuk halaman ini?");

      if( konf == true ){
        document.write("Password tersimpan");
        return true;
      }

      return false;
    }
  </script>   
</body>
</html>
```

Jika *user* mengklik tombol OK, maka jendela confirm() mengembalikan nilan **true** dan mengeluarkan hasil "Password tersimpan". Jika *user* mengklik tombol Cancel, maka jendela confirm() akan mengembalikan nilai **false**.

