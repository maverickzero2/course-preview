## Format Tampilan Teks

Dalam penulisan HTML kita akan sangat sering menggunakan format teks di dalamnya. Kita dapat menandai bagian-bagian tertentu dari sebuah teks HTML seperti cetak tebal, cetak miring dan lain-lain.

Berikut ini beberapa diantara elemen untuk format teks yang paling sering digunakan:

* `<b>` - mencetak tebal teks.
* `<strong>` - menandai teks penting.
* `<i>` - mencetak miring teks.
* `<em>` - memberikan penekanan pada teks.
* `<mark>` - menandai teks dengan sorotan.
* `<small>` - membuat teks lebih kecil dari ukuran bawaan.
* `<del>` - memberi coretan pada teks.
* `<ins>` - mengarisbawahi teks.
* `<sub>` - membuat tulisan lebih bawah dari garis datar tulisan.
* `<sup>` - membuat tulisan lebih atas dari garis datar tulisan.


```html runner-html
<!DOCTYPE html>
<html>
	<head>
		<title>Belajar HTML</title>
	</head>
	<body>
	    <b>mencetak tebal</b> <br />
		<strong>teks penting</strong> <br />
		mencetak <i>miring</i> teks <br />
		memberi <em>penekanan</em> pada teks <br />
		<mark>menandai teks</mark><br />
		teks <small>lebih kecil</small> dari default <br />
		<del>coretan</del> pada teks <br />
		<ins>mengarisbawahi</ins> teks <br />
		tulisan <sub>di bawah</sub> garis <br />
		tulisan <sup>di atas</sup> garis
	</body>
</html>
```

