## div dan span

### Tag `<div>`

Kita akan perlu membagi konten pada dokumen html berdasarkan konteksnya. Untuk itu kita dapat menggunakan tag `<div>`. Tag ini tidak memiliki kegunaan khusus seperti halnya tag lain yang sudah kita pelajari sebelumnya. Bila kita menggunakan tag `<div>` dia tidak akan menampilkan perbedaan dengan bila kita tidak menggunakan tag div.

```html
<h1>Belajar HTML Dasar</h1>
    
<p>HTML adalah bahasa yang digunakan untuk membuat website. 
Setiap website yang ada di dunia ini dibangun menggunakan 
bahasa yang dinamakan HTML. HTML menjadi sebuah kerangka 
membentuk struktur sebuah halaman website.
</p>

<p>copyright 2018 Nyankoders</p>
```

```html
<div id="header">
	<h1>Belajar HTML Dasar</h1>
</div>

<div id="content">
	<p>HTML adalah bahasa yang digunakan untuk membuat website. 
    Setiap website yang ada di dunia ini dibangun menggunakan 
    bahasa yang dinamakan HTML. HTML menjadi sebuah kerangka 
    membentuk struktur sebuah halaman website.</p>
</div>    

<div id="footer">
	<p>copyright 2018 Nyankoders</p>
</div>
```

Kode pertama dan kode kedua di atas akan menghasilkan tampilan yang sama persis bila dibuka di browser. Namun kode kedua akan lebih mudah dibaca pada format html karena setiap bagian dokumen sudah dikelompokkan berdasarkan konteksnya. Selain itu penggunaan `div` akan memudahkanmu saat menambahkan `style` nantinya. Penjelasan `style` akan dibahas di materi CSS.

### Tag `<span>`

 Sama seperti tag `<div>`, tag `<span>` pun tidak memiliki fungsi khusus. Tag ini biasanya digunakan bila kita ingin memberikan perlakuan khusus pada bagian tertentu pada teks.

 ```html
<p><span class="definisi">HTML adalah bahasa yang digunakan untuk 
membuat website.</span> Setiap website yang ada di dunia ini 
dibangun menggunakan bahasa yang dinamakan HTML. HTML menjadi 
sebuah kerangka membentuk struktur sebuah halaman website.</p>
 ```

### Sifat `<div>` dan `<span>` 

Elemen `<div>` bersifat *block*. Sehingga semua konten yang ada di dalam elemen ini akan ditampilkan di dalam blok tersendiri. Sedangkan elemen `<span>` bersifat *inline*. Semua konten yang ada di dalam tag ini tetap ditampilkan dalam baris yang sama dengan konten sebelum dan sesudahnya.