## Membuat Paragraf

Elemen `<p>` mendefinisikan sebuah konten paragraf. Konten teks dari paragraf harus ditulis diantara tag pembuka `<p>` dan tag penutup `</p>`.

```html
<!DOCTYPE html>
<html>
   <head>
      <title>Paragraph Example</title>
   </head>
   <body>
      <p>Ini adalah paragraf satu.</p>
      <p>Ini adalah paragraf dua.</p>
   </body>	
</html>
```

Bila kita membuat baris baru, spasi atau tab di dalam konten elemen, akan tetap ditampilkan sebagai satu spasi.

```html
<p>Ini      adalah sebuah 
paragraf.</p>
```

Kode HTML di atas tetap ditampilkan dalam satu baris tanpa ada jarak spasi lebih dari satu di dalamnya. Bila kita hendak membuat baris baru di dalam sebuah teks, maka gunakanlah tag `<br />`.

```html runner-html
<!DOCTYPE html>
<html>
   <head>
      <title>Line Break  Example</title>
   </head>
   <body>
      <p>Halo, Coders!<br />
         Untuk membuat baris baru, gunakan br<br />
         Selamat<br />
         mencoba!</p>
   </body>	
</html>
```

