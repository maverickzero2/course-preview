## List Bersarang

Pada beberapa kasus adakalanya kita perlu membuat daftar subpoin di dalam poin list. Untuk itu kita dapat membuat list di dalam list (list bersarang/nested list). Sebuah ordered list dapat disimpan di dalam unordered list dan begitu pula sebaliknya. 

Menuliskan kode untuk list di dalam list juga sangat sederhana, yakni dengan langsung menyimpan sublist ke dalam salahsatu elemen `<li>` dari list induknya.

```html runner-html
<ul>
	<li>Pakaian
		<ul>
			<li>Atasan</li>
			<li>Outerwear</li>
			<li>Bawahan</li>
			<li>Baju Koko</li>
			<li>Pakaian Dalam</li>
			<li>Pakaian Tidur</li>
		</ul>
	</li>
	<li>Sepatu
		<ul>
			<li>Sepatu Derbies</li>
			<li>Sneakers & Skate</li>
			<li>Sepatu Olahraga</li>
			<li>Sandal & Flip Flop</li>
			<li>Loafers</li>
			<li>Boots</li>
		</ul>
	</li>	
</ul>
```

Perhatian bahwa sublist disimpan di dalam elemen `<li>`. Artinya tag tutup `<li>` ditulis setelah menutup elemen sublist. Berikut adalah contoh penulisan sublist yang SALAH:

```html
<ul>
	<li>Pakaian</li>
	<ul>
		<li>Atasan</li>
		<li>Outerwear</li>
		<li>Bawahan</li>
		<li>Baju Koko</li>
		<li>Pakaian Dalam</li>
		<li>Pakaian Tidur</li>
	</ul>
</ul>
```

 Perhatikan bahwa tidak boleh ada anak elemen lain di bawah `<ul>` selain `<li>`. Pada contoh di atas kita menyimpan tag `<ul>` di dalam tag `<ul>` induknya, dan itu adalah contoh yang kurang tepat.