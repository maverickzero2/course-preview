## Membuat List Menyamping

Kita dapat mengubah tampilan list yang asalnya ke bawah menjadi ke samping (horizontal list) dengan mengubah style property `display` dari elemen `<li>` menjadi `inline` atau `inline-block` (elemen `<li>` termasuk elemen *block*). Teknik ini biasanya digunakan untuk membuat menu halaman yang ditampilkan ke samping.

```html runner-html
<style>
ul li {
	display: inline;
	margin-left: 15px;
	margin-right: 15px; }
</style>

<ul>
	<li>Nasi goreng</li>
	<li>Ayam goreng</li>
	<li>Nasi kuning</li>
	<li>Bubur</li>
</ul>
```

Akan tetapi, begitu mengubah nilai properti `display` menjadi `inline`, penanda daftar akan langsung hilang, dan setiap elemen dalam daftar akan saling bergabung. Idealnya, kita harus memberikan jarak secara manual menggunakan `padding` ataupun `margin`.
