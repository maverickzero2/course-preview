## Struktur List

Dalam HTML, elemen list atau daftar terdiri dari 2 jenis, yaitu *ordered list* dan *unordered list*. *Ordered list* adalah list yang menggunakan urutan karakter seperti angka atau huruf, sedangkan *unordered list* adalah list yang menggunakan simbol yang sama untuk setiap poin listnya. Letak perbedaannya hanya pada tag pembuka dan penutupnya saja, dimana *ordered list* menggunakan tag `<ol></ol>` dan *unordered list* menggunakan tag `<ul></ul>`. 

Untuk menuliskan poin-poin listnya kita menggunakan tag `<li></li>` dan disimpan di dalam elemen list. 

```html runner-html
<h1> Resep Pancake</h1>

<h2>Bahan</h2>
<ul>
	<li>250gr tepung terigu</li> <br>
	<li>1 butir telur</li> <br>
	<li>1 1/2 gelas susu</li> <br>
	<li>Susu bubuk putih</li> <br>
	<li>1 sdm baking powder</li> <br>
</ul>

<h2>Langkah</h2>
<ol>
	<li>Masukkan semua bahan ke dalam satu wajan 
	dan aduk sampai rata. Diamkan selama 10 menit</li>
	<li>Panaskan wajan anti lengket dengan api 
	sedang (tanpa minyak)</li>
	<li>Tuangkan 1-2 sendok makan adonan membentuk 
	bulatan di tengah wajan</li>
	<li>Balik pancake sebentar dan angkat</li>
	<li>Pancake dapat dilesi coklat atau madu</li>
</ol>
```

Kode di atas adalah contoh struktur penulisan dari ordered list dan unorderd list. Dimana bahan menggunakan unordered list dan langkah-langkah resep menggunakan ordered list `<ol>`. Setiap poin daftar ditulis di dalam elemen `<li>`.

