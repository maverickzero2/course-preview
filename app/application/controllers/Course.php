<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Symfony\Component\Yaml\Yaml;

class Course extends CI_Controller {

	public $courses_path = "";
	public $course_folder = "courses/";

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		define('VERSION', file_get_contents('version'));

		if($this->session->userdata('course'))
			$this->courses_path = $this->course_folder.$this->session->userdata('course');
	}

	function index($course = false)
	{
		if($course){
			redirect('course/lesson/'.$course);
		}

		// get course topics folder list
		$folders = array_filter(directory_map($this->course_folder, 1), function($var){
			return file_exists($this->course_folder.$var.'course.yml');
		});

		$data['title'] = 'Choose Course';
		$data['courses'] = $folders;
		$this->_render('courses', $data);
	}

	/*
	 * Show lesson list
	 */
	function lesson($course = false, $topic = false, $lessonSlug = false)
	{
		if(! $course) redirect('course/choose_course');
		
		$this->courses_path = $this->course_folder.$course.'/';
		$lesson_tree = $this->_lesson_tree();
		// print_r($lesson_tree);
		
		// get first topic
		if(! $topic) {
			reset($lesson_tree);
			$topic = key($lesson_tree);
		}

		// prepare lesson content and attributes
		// if lessonSlug not defined, choose first file
		reset($lesson_tree[$topic]['lessons']);
		if(!$lessonSlug) $lessonSlug = key($lesson_tree[$topic]['lessons']);

		// get file content
		$content = file_get_contents($this->courses_path.$topic.'/'.$lessonSlug.'.md');

		// separate yaml and content
		if(strpos($content, "---\n") !== false){
			list($attributes, $content) = explode("---\n", $content, 2);
			$currentLesson = Yaml::parse($attributes);
			$currentLesson['slug'] = $lessonSlug;
		} else {
			// at least get first row as title
			list($attributes, $content) = explode("\n", $content, 2);
			$currentLesson['title'] = str_replace('#','', $attributes);
			$currentLesson['slug'] = $lessonSlug;
		}

		// parse content
		$Parsedown = new ParsedownExtra();
		$currentLesson['content'] = $Parsedown->text($content);

		// change images path
		$pattern = '../images/';
		$replace = site_url($this->courses_path.'images/');
		$currentLesson['content'] = str_replace($pattern, $replace, $currentLesson['content']);

		// load view
		$data['title'] =  $currentLesson['title'];
		$data['lesson_tree'] = $lesson_tree;
		$data['course'] = $course;
		$data['topic'] = $lesson_tree[$topic];
		$data['lessons'] = $lesson_tree[$topic]['lessons'];
		$data['lesson'] = $currentLesson;

		$this->_render('lesson', $data);
	}

	/*
	 * Show lesson list
	 */
	function printing($topic = false)
	{
		if(empty($this->courses_path))
			redirect('course/choose_course');

		// get lesson files list
		chdir($this->courses_path.$topic);
		$files = glob('??-*');
		if(empty($files)) show_error('Topic must have at least one lesson file and named such as 01-some-title-for-your-lesson.md');
		// print_r($files);
		
		// prepare lesson list
		$lessons = [];
		$content = '';
		foreach ($files as $file){
			// separate number and name
			list($number, $slug) = explode('-', $file, 2);

			$title = str_replace(["-",".md"], [" ",""], $slug);

			// get slug and name
			$lessons = [
				'file' => $file,
				'number' => $number,
				'slug' => str_replace(".md", "", $slug),
				'title' => $slug == strtolower($slug) ? ucwords($title) : $title
			];

			$content .= file_get_contents($file)."\n";
		}
		// print_r($lessons);

		// parse content
		$Parsedown = new ParsedownExtra();
		$lesson['content'] = $Parsedown->text($content);

		// change images path
		$pattern = '../images/';
		$replace = site_url($this->courses_path.'images/');
		$lesson['content'] = str_replace($pattern, $replace, $lesson['content']);

		// set topic attributes
		list($number, $topicSlug) = explode('-', $topic, 2);
		$topic = [
			'folder' => $topic,
			'slug' => $topicSlug,
			'title' => str_replace("-", " ", $topicSlug)
		];
		if($topicSlug == strtolower($topicSlug))
			$topic['title'] = ucwords($topic['title']);

		$lesson['title'] = $topic['title'];

		// load view
		$data['title'] =  $lesson['title'];
		$data['lessons'] = $lessons;
		$data['lesson'] = $lesson;
		$data['topic'] = $topic;
		$data['print'] = true;
		$this->_render('lesson', $data);
	}

	function _lesson_tree()
	{
		$folders = $this->_get_file_tree($this->courses_path);
		$topics = [];
		foreach ($folders as $folder)
		{
			list($number, $slug) = explode('-', trim($folder,'/'), 2);
			$title = str_replace("-", " ", $slug);

			// prepare lesson list first
			$lessons = [];
			$lesson_folders = $this->_get_file_tree($this->courses_path.$folder);
			foreach ($lesson_folders as $lesson_file)
			{
				list($lesson_number, $lesson_slug) = explode('-', trim($lesson_file,'/'), 2);
				$lesson_slug = str_replace('.md', "", $lesson_slug);
				$lesson_title = str_replace("-", " ", $lesson_slug);

				// get slug and name
				$lessons[trim($lesson_file, '.md')] = [
					'file'	=> $lesson_file,
					'number'=> $lesson_number,
					'slug' 	=> $lesson_slug,
					'title' => $lesson_slug == strtolower($lesson_slug) 
								? ucwords($lesson_title) 
								: $lesson_title
				];
			}

			// compact lesson to topic
			$topics[trim($folder, '/')] = [
				'folder'	=> $folder,
				'number'	=> $number,
				'slug' 		=> $slug,
				'title' 	=> $slug == strtolower($slug) ? ucwords($title) : $title,
				'lessons' 	=> $lessons
			];
		}
		// print_r($topics);exit;

		return $topics;
	}

	function _get_file_tree($path)
	{
		$folders = directory_map($path, 1);
		sort($folders);
		$folders = array_filter($folders, function($v){
			return preg_match('/[0-9]+-/', $v);
		});
		
		return $folders;
	}

	function _render($template = 'page', $data)
	{
		$data['body'] = $this->load->view($template, $data, true);
		$this->load->view('template/layouts/basic', $data);
	}

}
