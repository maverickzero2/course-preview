<?php defined('BASEPATH') or exit('No direct script access allowed');

use Symfony\Component\Yaml\Yaml;

class Book extends CI_Controller
{

    public $courses_path = "";
    public $course_folder = "courses/";

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');

        define('VERSION', file_get_contents('version'));
    }

    public function index($course = "")
    {
        if ($course) {
			$data['title'] = str_replace('-',' ', $course);
			$data['course'] = $course;
			$this->_render('book/content', $data);
        } else {
			// get course topics folder list
			$folders = array_filter(directory_map($this->course_folder, 1), function ($var) {
				return file_exists($this->course_folder . $var . 'course.yml');
			});
			
			$data['title'] = 'Choose Course';
			$data['courses'] = $folders;
			$this->_render('book/courses', $data);
		}
	}
	
	function getMenu($course){
		$tree = $this->_lesson_tree($course);
		echo $this->load->view('sidebar_menu', compact('tree'), true);
	}

	function getContent(){
		$path = $this->uri->segment_array();
		array_shift($path);
		$course_path = trim(implode('/', $path), '.html');
        $md = file_get_contents($this->course_folder.$course_path.'.md');
        echo $md;

		// $Parsedown = new ParsedownExtra();
		// echo $Parsedown->text($md);
	}

    /*
     * Show lesson list
     */
    public function export($course = false)
    {
		$coursePath = 'output/'.$course;
		$viewPath = $coursePath.'/views/';
        if (!file_exists($viewPath)) {
			mkdir($viewPath, 0777, true);
			mkdir($coursePath.'/assets/', 0777, true);
		}
		
		$tree = $this->_lesson_tree($course);
		$menu = $this->load->view('sidebar_menu', compact('tree'), true);
		file_put_contents($coursePath.'/assets/menu.html', $menu);
	
		// export contents
        $Parsedown = new ParsedownExtra();
        foreach ($tree as $topic) {
			foreach ($topic['lessons'] as $lesson) {
				$md = file_get_contents($this->course_folder.$course.'/'.$topic['folder'].$lesson['file']);
				$content = $Parsedown->text($md);
				if(!file_exists($viewPath.$topic['folder'])){
					mkdir($viewPath.$topic['folder']);
				}
				$output_file = $viewPath.$topic['folder'].$lesson['number'].'-'.$lesson['slug'].'.html';
				echo "writing <code>".$output_file.'</code> ';
				if(file_put_contents($output_file, $content))
					echo " <b>[DONE]</b><br>";
				else
					echo " <b style='color:red'>[FAIL]</b><br>";
            }
        }
    }

    public function _lesson_tree($course = false)
    {
		if($course)
			$this->courses_path = $this->course_folder.$course.'/';
		
			$folders = $this->_get_file_tree($this->courses_path);
        $topics = [];
        foreach ($folders as $folder) {
            list($number, $slug) = explode('-', trim($folder, '/'), 2);
            $title = str_replace("-", " ", $slug);

            $lessons = [];
            $lesson_folders = $this->_get_file_tree($this->courses_path . $folder);
            foreach ($lesson_folders as $lesson_file) {
                list($lesson_number, $lesson_slug) = explode('-', trim($lesson_file, '/'), 2);
                $lesson_slug = str_replace('.md', "", $lesson_slug);
                $lesson_title = str_replace("-", " ", $lesson_slug);

                // get slug and name
                $lessons[] = [
                    'file' => $lesson_file,
                    'number' => $lesson_number,
                    'slug' => $lesson_slug,
                    'title' => $lesson_slug == strtolower($lesson_slug)
                    ? ucwords($lesson_title)
                    : $lesson_title,
                ];
            }

            // get slug and name
            $topics[] = [
                'folder' => trim($folder, '.md'),
                'number' => $number,
                'slug' => $slug,
                'title' => $slug == strtolower($slug) ? ucwords($title) : $title,
                'lessons' => $lessons,
            ];
        }
        // print_r($topics);exit;

        return $topics;
    }

    public function _get_file_tree($path)
    {
        $folders = directory_map($path, 1);
        sort($folders);
        $folders = array_filter($folders, function ($v) {
            return preg_match('/[0-9]+-/', $v);
        });

        return $folders;
    }

    public function _render($template = 'page', $data)
    {
        $data['body'] = $this->load->view($template, $data, true);
        $this->load->view('template/layouts/ajax_basic', $data);
    }

}
