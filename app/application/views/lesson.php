<!-- SECTION LESSON CONTAINER -->
<section class="lesson-container">

	<div id="wrapper">
		<a href="#menu-toggle" class="btn btn-lg" id="menu-toggle">
			<span class="glyphicon glyphicon-menu-hamburger"></span>
		</a>

		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<?php foreach ($lesson_tree as $topictree): ?>
					<li>
						<label><?= $topictree['title']; ?></label>
						<ul class="list-unstyled">					
						<?php foreach ($topictree['lessons'] as $lessontree): ?>
							<li>
								<a class="<?php echo $lesson['slug'] == $lessontree['number'].'-'.$lessontree['slug'] ? 'active' : ''; ?>" href="<?= site_url('course/'.$course.'/'.$topictree['folder'].$lessontree['number'].'-'.$lessontree['slug']); ?>"><?= $lessontree['title']; ?></a>
							</li>
						<?php endforeach; ?>
						</ul>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<!-- /#sidebar-wrapper -->

	<div class="container-fluid">
		<div class="row">
			<!--
			<div class="col-sm-3 lesson-sidebar">
				<div class="lesson-detail">
					<a href="<?= site_url(); ?>" title="Back to course">
						<div class="lesson-category">
							&laquo;
							<span class="lesson-category-title"><?= $topic['title']; ?></span>
						</div>
					</a>
				</div>
				
				<div class="lesson-menu">
					<ol>
						<?php $current = 0;
						foreach ($lessons as $lessonIndex => $lessonList): ?>
						<?php
							if($lessonList['number'].'-'.$lessonList['slug'] == $lesson['slug']){
								$active = 'active';
								$current = $lessonIndex;
							} else {
								$active = "";
							}
						?>
						<li class="<?= $active; ?>">
							<a href="<?= site_url("course/{$topic['folder']}/{$lessonList['number']}-{$lessonList['slug']}"); ?>"><?= $lessonList['title']; ?></a>
						</li>
						<?php endforeach; ?>
					</ol>
				</div>
			</div>
		-->

		<div class="col-md-6 col-md-offset-3">
			<div class="content-wrapper">
				<div class="content-header">
					<span class="content-info hidden-print">Lesson <?= $current+1; ?> of <?= count($lessons); ?></span>
					<h1 class="content-title"><?= $lesson['title']; ?></h1>
				</div>

				<div class="content-body">
					<?= $lesson['content']; ?>
				</div>

				<hr>

				<nav style="margin: 40px 0 60px;" class="hidden-print">
					<ul class="pager">
						<?php if(isset($lessons[$current-1])): ?>
							<li class="previous">
								<a href="<?= site_url("course/{$topic['folder']}/{$lessons[$current-1]['number']}-{$lessons[$current-1]['slug']}"); ?>">
									<span aria-hidden="true">&larr;</span>
									Previous
								</a>
							</li>

						<?php endif; ?>

						<?php if(isset($lessons[$current+1])): ?>
							<li class="next">
								<a href="<?= site_url("course/{$topic['folder']}/{$lessons[$current+1]['number']}-{$lessons[$current+1]['slug']}"); ?>">
									Next 
									<span aria-hidden="true">&rarr;</span>
								</a>
							</li>
						<?php endif; ?>
					</ul>
				</nav>
			</div>
		</div>

	</div>
</div>


<div class="lesson-fly hidden-print">
	<div class="media">
		<div class="media-left">
			<a href="https://devschool.id/"><img src="http://www.produktif.net/wp-content/uploads/2018/03/ads-kelas.jpg" class="fly-img"></a>
		</div>
		<div class="media-body">
			<div class="fly-title"><a href="https://devschool.id/">Ingin Belajar Coding Dibimbing oleh Instruktur?</a></div>
			<div class="fly-label">devschool.id</div>
		</div>
	</div>
</div>
</section>
<!-- END: SECTION LESSON CONTAINER -->