<!-- FOOTER -->
<!-- <footer class="cp-footer" id="contact">
	<div class="container">
		
		<div class="footer-copyright text-center">
			<h4>Developer School</h4>

			<p>Jalan Cipedes Tengah 1 No.27 Sukajadi Bandung<br />(<a href="https://www.google.co.id/maps/place/CodePolitan/@-6.8795127,107.5878933,17z/data=!3m1!4b1!4m5!3m4!1s0x2e68e68e46d527c3:0x80c53eeb41601c6e!8m2!3d-6.879518!4d107.590082?hl=id" target="_blank">Lihat peta</a>)</p>

			<p>Senin s/d Jumat<br />08.00 WIB s/d 20.00 WIB</p>

			<p>Call: +62 815-1373-9905​⁠​<br />WhatsApp: +62 815-1373-9905​⁠​<br />Mail: school@codepolitan.com</p>

			<div class="footer-sosmed">
				<div class="follow-icon">
					<a href="https://www.facebook.com/codepolitan/" class="follow-facebook record-data" data-action="follow-facebook" data-position="home-social-media" target="_blank"><span class="fa fa-facebook-official"></span></a>
					<a href="https://twitter.com/codepolitan" class="follow-twitter record-data" data-action="follow-twitter" data-position="home-social-media" target="_blank"><span class="fa fa-twitter"></span></a>
					<a href="https://plus.google.com/+Codepolitan" class="follow-google-plus record-data" data-action="follow-google-plus" data-position="home-social-media" target="_blank"><span class="fa fa-google-plus"></span></a>
					<a href="https://www.youtube.com/channel/UCf9YmYfSG0d_vxxZDMJfElA" class="follow-youtube record-data" data-action="follow-youtube" data-position="home-social-media" target="_blank"><span class="fa fa-youtube-play"></span></a>
					<a href="https://www.linkedin.com/company/codepolitan" class="follow-linkedin record-data" data-action="follow-linkedin" data-position="home-social-media" target="_blank"><span class="fa fa-linkedin-square"></span></a>
					<a href="https://www.instagram.com/codepolitan/" class="follow-instagram record-data" data-action="follow-instagram" data-position="home-social-media" target="_blank"><span class="fa fa-instagram"></span></a>
				</div>
			</div>

			Copyright © 2017 <a href="https://www.codepolitan.com/">CodePolitan</a>. All rights reserved.
		</div>
			
	</div>
</footer>
--><!-- END: FOOTER -->



<!-- Bridge -->
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

<!-- Jquery Head -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<!-- put this runner code before prism or any syntax highlighting plugin -->
<?php if(! isset($print)): ?>
	<script id="runnerUrl" data-url="https://development.codepolitan.com" src="https://development.codepolitan.com/assets/js/cp_runner.js"></script>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/prism.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/components/prism-php.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/components/prism-java.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/components/prism-python.min.js"></script>

<script>
	$(".btn-scroll-to").click(function() {
		var href_data = $(this).attr("href");

		$('html, body').animate({
			scrollTop: $(href_data).offset().top - 46
		}, 1000);

		return false;
	});

	$('form').submit(function(e){
		$(this).find('button[type=submit]').prop( "disabled", true ).html('<span class="fa fa-spinner rotate"></span> Please wait..');
	});


  	/*
	$('#general_popup').modal('show');
	*/

	$("iframe").load(function() {
		$(this).height( $(this).contents().find("body").height() );
	});

	var menu_open = Cookies.get('menu_open');
	if(menu_open == 'opened'){
		$("#wrapper").addClass("toggled");
		var activeScroll = $("#sidebar-wrapper a.active").offset().top - $("#sidebar-wrapper a.active").offsetParent().offset().top - 80;
		$("#sidebar-wrapper").scrollTop(activeScroll);
	}

	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");

		if(menu_open == 'opened')
			Cookies.set('menu_open', 'closed');
		else
			Cookies.set('menu_open', 'opened');
	});
</script>
