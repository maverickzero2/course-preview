<section class="content-wrapper" style="margin-top:60px">
	<div class="container">

		<div class="content-error">
			<div class="row">
				<div class="col-md-6">
					<h1>404</h1>
					<p>Konten yang kamu cari tidak ditemukan di CodePolitan.</p>

					<a href="<?php echo base_url(); ?>" class="btn btn-lg btn-danger"><span class="fa fa-long-arrow-left"></span> Kembali Ke Home</a>
				</div>
			</div>
		</div>

	</div>
</section>