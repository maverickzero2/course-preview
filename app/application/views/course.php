<div class="container-fluid course-wrapper">
	<div class="row">

		<div class="col-sm-6 lesson-sidebar">
			<div class="course-detail">
				<div class="course-detail-inner">
					<div class="course-info">
						<img src="<?= $course['cover']; ?>" class="course-image" width="100%" alt="">
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 course-content main-content clearfix">
			<div class="content-body">
				<h1 class="course-title"><?= $course['title']; ?></h1>
				<span class="course-description"><?= $course['description']; ?></span>

				<br><br>
				<ol class="lesson-loop">
					<?php foreach ($topics as $number => $topic): ?>
						<li class="">
							<a href="<?= site_url("course/{$topic['folder']}"); ?>">
								<?= $topic['title']; ?>
							</a>
							&bull;
							<a href="<?= site_url("print/{$topic['folder']}"); ?>" target="_blank">
								(Print Mode)
							</a>

							<span class=" lesson-status">0%</span>
						</li>
					<?php endforeach; ?>
				</ol>
			</div>
		</div>
	</div>
</div>