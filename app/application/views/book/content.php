<!-- SECTION LESSON CONTAINER -->
<section class="lesson-container">

	<div id="wrapper">
		<a href="#menu-toggle" class="btn btn-lg" id="menu-toggle">
			<span class="glyphicon glyphicon-menu-hamburger"></span>
		</a>

		<div id="sidebar-wrapper">
			<ul class="sidebar-nav"></ul>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">

			<div class="col-md-6 col-md-offset-3">
				<div class="content-wrapper">
					<div class="content-body" id="main-content">
						<!-- content here -->
					</div>
				</div>
			</div>

		</div>
	</div>

<!-- <div class="lesson-fly hidden-print">
	<div class="media">
		<div class="media-left">
			<a href="https://devschool.id/"><img src="http://www.produktif.net/wp-content/uploads/2018/03/ads-kelas.jpg" class="fly-img"></a>
		</div>
		<div class="media-body">
			<div class="fly-title"><a href="https://devschool.id/">Ingin Belajar Coding Dibimbing oleh Instruktur?</a></div>
			<div class="fly-label">devschool.id</div>
		</div>
	</div>
</div> -->
</section>
<!-- END: SECTION LESSON CONTAINER -->
