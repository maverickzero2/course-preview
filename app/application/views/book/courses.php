<div class="container" style="margin-top: 80px;">
	<div class="row">
		<?php foreach ($courses as $course): ?>
			<div class="col-md-3">
				<div class="thumbnail" style="min-height: 160px;">
					<div class="caption">
						<p><a href="<?php echo site_url('book/'.$course); ?>" class="btn btn-primary btn-block" role="button">Open Course</a></p>
						<br>
						<h4><?php echo str_replace("-", " ", trim($course, '/')); ?></h4>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

<style>
.course {background: white;}
</style>